public class Triangle {
    private int height;
    private int base;

    public Triangle(int h, int b){
        this.height=h;
        this.base=b;
    }
    public int getHeight(){
        return this.height;
    }
    public int getBase(){
        return this.base;
    }
    public double getArea(){
        return (this.height*this.base)/2;
    }
    public String toString(){
        return "height: "+this.height+" base: "+this.base;
    }
}
